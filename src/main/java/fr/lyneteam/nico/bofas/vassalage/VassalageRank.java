package fr.lyneteam.nico.bofas.vassalage;

import org.bukkit.ChatColor;

public enum VassalageRank {
	RANK0("Paysan", ChatColor.GRAY),
	RANK1("Employeur", ChatColor.WHITE),
	RANK2("Petit seigneur", ChatColor.BLUE),
	RANK3("Seigneur", ChatColor.AQUA),
	RANK4("Roi", ChatColor.GOLD);
	
	public static VassalageRank getByNumber(int number) {
		return valueOf("RANK" + number);
	}
	
	private final String name;
	private final ChatColor color;
	
	private VassalageRank(String name, ChatColor color) {
		this.name = name;
		this.color = color;
	}

	public final String getName() {
		return this.name;
	}

	public final ChatColor getColor() {
		return this.color;
	}
	
	public final int getNumber() {
		return Integer.parseInt(this.name().substring(4));
	}
}