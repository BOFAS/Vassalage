package fr.lyneteam.nico.bofas.vassalage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

public class VassalageInfo {
	private final VassalageManager vassalage;
	private final String uuid;
	
	public VassalageInfo(VassalageManager vassalage, String uuid) {
		this.vassalage = vassalage;
		this.uuid = uuid;
	}
	
	public final String getUUID() {
		return this.uuid;
	}
	
	public void setLiege(String uuid) {
		this.vassalage.set(this.uuid, uuid);
	}
	
	public final VassalageInfo getLiege() {
		return this.vassalage.getInfo(this.vassalage.get(this.uuid));
	}
	
	@Deprecated
	public final String getMasterAsString() {
		return this.vassalage.get(this.uuid);
	}
	
	public void addVassal(String uuid) {
		this.vassalage.getInfo(uuid).setLiege(this.uuid);
	}

	public final List<VassalageInfo> getVassals() {
		List<VassalageInfo> vassals = new ArrayList<VassalageInfo>();
		for (Entry<String, String> entry : this.vassalage.getEntries()) if (entry.getValue().equals(this.uuid)) vassals.add(new VassalageInfo(this.vassalage, entry.getKey()));
		return vassals;
	}
	
	@Deprecated
	public final List<String> getVassalsAsListString() {
		List<String> vassals = new ArrayList<String>();
		for (Entry<String, String> entry : this.vassalage.getEntries()) if (entry.getValue().equals(this.uuid)) vassals.add(entry.getKey());
		return vassals;
	}
	
	public final List<VassalageInfo> getAllVassals() {
		List<VassalageInfo> vassals = new ArrayList<VassalageInfo>();
		for (Entry<String, String> entry : this.vassalage.getEntries()) if (entry.getValue().equals(this.uuid)) {
			VassalageInfo vassal = new VassalageInfo(this.vassalage, entry.getKey());
			vassals.add(vassal);
			for (VassalageInfo temporaly : vassal.getAllVassals()) if (!vassals.contains(temporaly)) vassals.add(temporaly);
		}
		return vassals;
	}
	
	@Deprecated
	public final List<String> getAllVassalsAsListString() {
		List<String> vassals = new ArrayList<String>();
		for (Entry<String, String> entry : this.vassalage.getEntries()) if (entry.getValue().equals(this.uuid)) {
			VassalageInfo vassal = new VassalageInfo(this.vassalage, entry.getKey());
			vassals.add(entry.getKey());
			for (String temporaly : vassal.getAllVassalsAsListString()) if (!vassals.contains(temporaly)) vassals.add(temporaly);
		}
		return vassals;
	}
	
	public final VassalageRank getRank() {
		int number = 0;
		for (VassalageInfo vassal : this.getVassals()) {
			int temporaly = vassal.getRank().getNumber() + 1;
			if (temporaly > number) number = temporaly;
		}
		return VassalageRank.getByNumber(number);
	}
}