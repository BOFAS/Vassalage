package fr.lyneteam.nico.bofas.vassalage;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import fr.lyneteam.nico.database.Database;

public class VassalageManager extends Thread {
	private final Database database;
	private final Map<String, String> last;
	private Map<String, String> old;
	private boolean working;
	
	public VassalageManager(Database database) {
		this.database = database;
		this.last = new HashMap<String, String>();
		this.working = true;
		this.start();
	}
	
	public void run() {
		try {
			this.get();
		} catch (Exception e) {
			System.exit(0); // Restart ! Cause could not get database data
		}
		this.working = false;
		while (true) try { // Security for crash
			this.save();
			sleep(10000);
		} catch (Exception exception) {
			// Ignore
		}
	}

	private void get() throws Exception {
		this.last.clear();
		ResultSet result = this.database.executeQuery("SELECT * FROM `vassalage`");
		while (result.next()) this.last.put(result.getString("key"), result.getString("value"));
		this.old = this.last;
	}

	private void save() throws Exception { // SAVE BEFORE CLOSE THE SERVER ! IMPORTANT
		if (this.working) return;
		this.working = true;
		List<String> temporaly = new ArrayList<String>();
		Map<String, String> clone = new HashMap<String, String>(this.last);
		for (Entry<String, String> data : clone.entrySet()) {
			String key = data.getKey(); // Could not be null
			String last = data.getValue(); // Could not be null
			String old = this.old.get(key);
			if (old == null) {
				this.database.execute("INSERT INTO `vassalage` (`key`, `value`) VALUES ('" + key + "','" + last + "');");
			} else if (!last.equals(old)) {
				this.database.execute("UPDATE `vassalage` SET `value`='" + last + "' WHERE `key`='" + key + "'");
			}
			temporaly.add(key);
		}
		for (String key : this.old.keySet()) {
			if (!temporaly.contains(key)) {
				this.database.execute("DELETE FROM `vassalage` WHERE `key`='" + key + "'");
			}
		}
		this.old = clone;
		this.working = false;
	}

	public final VassalageInfo getInfo(String uuid) {
		if (uuid == null) return null;
		return new VassalageInfo(this, uuid);
	}

	// Protected methods, do not use !
	
	protected Set<Entry<String, String>> getEntries() {
		return this.last.entrySet();
	}

	protected void set(String key, String value) {
		this.last.put(key, value);
	}

	protected final String get(String key) {
		return this.last.get(key);
	}
}