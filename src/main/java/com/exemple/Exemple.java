package com.exemple;

import fr.lyneteam.nico.bofas.vassalage.VassalageInfo;
import fr.lyneteam.nico.bofas.vassalage.VassalageManager;
import fr.lyneteam.nico.database.Database;
import fr.lyneteam.nico.database.DatabaseMysql;
import fr.lyneteam.nico.database.DatabaseUser;

public class Exemple implements DatabaseUser {
	private VassalageManager vassalage;
	
	public Exemple() {
		try {
			new DatabaseMysql(this, "example.com", 3306, "database", "username", "password", false);
		} catch (Exception exception) {
			System.exit(0); // Error
		}
	}
	
	public final VassalageManager getVassalageManager() { // Warn null while not loaded !
		return this.vassalage;
	}

	public void setDatabase(Database database) {
		this.vassalage = new VassalageManager(database);
		
		VassalageInfo info = this.vassalage.getInfo("an uuid");
		System.out.println("Master : " + info.getLiege().getUUID());
		System.out.println("Rank : " + info.getRank().getName() + "[" + info.getRank().getColor().name() + "]");
		info.setLiege("an other uuid");
		System.out.println("Master : " + info.getLiege().getUUID());
		System.out.println("Direct vassals : " + info.getVassals().size());
		System.out.println("Total vassals : " + info.getAllVassals().size());
		info.addVassal("an other...");
		System.out.println("Direct vassals : " + info.getVassals().size());
		System.out.println("Total vassals : " + info.getAllVassals().size());
	}
}